<?php
/**
 * Created by PhpStorm.
 * User: dgarkaev
 * Date: 10.11.18
 * Time: 16:02
 */

function ploterr($msg)
{
//    $img = imagecreatetruecolor ( 400 , 200 );

    header ('Content-Type: image/png');
    $im = @imagecreatetruecolor(800, 400)
    or die('Невозможно инициализировать GD поток');
    $text_color = imagecolorallocate($im, 200, 200, 200);

    $txtget = json_encode($_GET);//implode("\n",$_GET);
    $txtget = str_replace(['{','}'],'',$txtget);
    $txtget =str_replace(",","\n",$txtget);;

    $msg = $msg."\n".$txtget;
    $txt=explode("\n", $msg);
    $y=5;
    foreach ($txt as $item) {
        imagestring($im, 3, 5, $y,  $item, $text_color);
        $y+=14;
    }

    imagepng($im);
    imagedestroy($im);
}