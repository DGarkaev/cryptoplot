<?php

require_once 'config.php';
//header('Content-type: image/png');

$LIMIT=240;
if (!empty($_GET["limit"]))
    $LIMIT=$_GET["limit"];

$FROM='BTC';
$TO='RUB';

$E=array('exmo','yobit','dsx');

$W=1200;
if (!empty($_GET["w"]))
    $W=$_GET["w"];
$H=600;
if (!empty($_GET["h"]))
    $H=$_GET["h"];

$plot=array();
//получаем данные с cryptocompare
foreach ($E as $ex)
{
    $TO2=$TO;
    if ($ex == 'yobit' && $TO == 'RUB')
        $TO2='RUR';
    $url = $cc."?fsym=$FROM&tsym=$TO2&limit=$LIMIT&e=$ex";
    $feed = file_get_contents($url, false, stream_context_create($options));
    $ccdata[] = json_decode($feed, true);
    unset($feed);
    $plot[]= <<<PLOT
"-" using 1:2:3 with filledcurves title "$ex"
PLOT;
}

$plotstr='plot '.implode(', ', $plot);

$image_file = tempnam("./tmp","gpout_");

$gnuplot_cmds = <<< GNUPLOTCMDS
set term png truecolor enhanced font arial 9 size $W, $H
#set term svg size $W, $H font "arial" dynamic mouse jsdir "js"

set output "$image_file"
#set title "Title"
#set lmargin 12
#set rmargin 12
#set key outside
set style fill transparent solid 0.5
set ylabel "Цена, р."
set grid x y
set xlabel "Дата"
set timefmt "%s"
set format x "%d/%m"
set xdata time
GNUPLOTCMDS;

$gnuplot_cmds .= "\n".$plotstr."\n";


$descriptorspec = array(
    0 => array('pipe', 'r'),
    1 => array('pipe', 'w'),
    2 => array('pipe', 'r')
);

$process = proc_open('gnuplot', $descriptorspec, $pipes);

if (!is_resource($process)) {
    throw new \Exception('Unable to run GnuPlot');
}

$stdin = $pipes[0];
$stdout = $pipes[1];

fwrite($stdin, $gnuplot_cmds);

//передаем данные для графиков
foreach ($ccdata as $data)
{
    foreach ($data['Data'] as $d)
    {
        $ss=$d['time'].' '.$d['high'].' '.$d['low']."\n";
        fwrite($stdin, $ss);
    }
    fwrite($stdin, "e\n");
}


$gnuplot_cmds="quit\n";
fwrite($stdin, $gnuplot_cmds);
proc_close($process);
#header("Content-Type: image/svg+xml");
header("Content-Type: image/png");
readfile($image_file);
//
// Clean up and exit
unlink($image_file);

?>