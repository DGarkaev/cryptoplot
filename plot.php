<?php

include_once 'dev.php';
require_once 'ploterr.php';
if (!function_exists('array_key_last')) {
    /**
     * Polyfill for array_key_last() function added in PHP 7.3.
     *
     * Get the last key of the given array without affecting
     * the internal array pointer.
     *
     * @param array $array An array
     *
     * @return mixed The last key of array if the array is not empty; NULL otherwise.
     */
    function array_key_last($array)
    {
        $key = NULL;

        if (is_array($array)) {

            end($array);
            $key = key($array);
        }

        return $key;
    }
}

/**
 * @param array $ma значение sma в этот день
 * @param array $a массив отклонений
 * @param int $t период на который считаем среднее из $a
 */
function avg($ma, $a, $t)
{
    $k = key($ma);
    $v = $ma[$k];
    $a0 = array_slice($a, 0, $t, true);
    $b1 = [];
    $b2 = [];
    foreach ($a0 as $i) {
        if ($i > $v) $b1[] = $i;
        else $b2[] = $i;
    }
    $sum1 = array_sum($b1) / count($b1);// + 0.3;
    $sum2 = array_sum($b2) / count($b2);// - 0.3;
    return array('sum1' => $sum1, 'sum2' => $sum2);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
date_default_timezone_set('Asia/Omsk');
$t = date_create();

$cc = 'https://min-api.cryptocompare.com/data/histohour';

$cl = 'http://10.132.0.2/api/historyRates';
$ckz = 'http://10.132.0.2/srom/getHistoryRate';
$dnforex = 'http://10.132.0.2/api/ratesForex';
if (!is_null($GLOBALS['dev'])) {
    $params = array(
        'username' => 'DmitryG',
        'password' => 'lvbnhbq532ufhmrftd333'
    );
    $aHTTP = array(
        'http' => // Обертка, которая будет использоваться
            array(
                'method' => 'POST', // Метод запроса
                // Ниже задаются заголовки запроса
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params),
                'timeout' => 20
            )
    );
    $sURL = 'https://dealsbot.tk/login';
    $context = stream_context_create($aHTTP);
    $token = json_decode(file_get_contents($sURL, false, $context));
    $cl = 'https://dealsbot.tk/manyapi/api/historyRates';
    $ckz = 'https://dealsbot.tk/manyapi/srom/getHistoryRate';
    $dnforex = 'https://dealsbot.tk/manyapi/api/ratesForex';
}

$forex = 'https://min-api.cryptocompare.com/data/histohour';

$options = [
    'http' => [
        'method' => 'GET',
        'timeout' => 10
    ]
];

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// TESTING
//if (!is_null($GLOBALS['dev']))
//{
//    $_GET = array(
//        'le' => 'exmo',
//        'lc' => 'rub',
//        're' => 'dsx',
//        'rc' => 'rub',
//        'day' => '7',
//        'mode' => 'd'
//    );
//}


$MODE = 'g';
if (!empty($_GET["mode"]))
    $MODE = $_GET["mode"];
//if ($MODE != 'g') $MODE = 't';
$_GET["mode"] = $MODE;

$DAY = 15;
if (!empty($_GET["day"]))
    $DAY = $_GET["day"];
//if ($DAY<7) $DAY=7;
$_GET["day"] = $DAY;

$MA = 14;
if (!empty($_GET["ma"]))
    $MA = $_GET["ma"];
$_GET["ma"] = $MA;

//т.к. получаем часовые данные, в дне 24 часа => вычисляем лимит
$LIMIT = 24 * ($DAY + $MA);
if (!empty($_GET["limit"]))
    $LIMIT = $_GET["limit"];
$_GET["limit"] = $LIMIT;

$FROML = 'BTC';
$_GET["lfrom"] = $FROML;

$FROMR = 'BTC';
$_GET["rfrom"] = $FROMR;

$TOL = 'RUB';
if (!empty($_GET["lc"]))
    $TOL = strtoupper($_GET["lc"]);
$_GET["lc"] = $TOL;

$TOR = 'RUB';
if (!empty($_GET["rc"]))
    $TOR = strtoupper($_GET["rc"]);
$_GET["rc"] = $TOR;


$EL = 'exmo';
if (!empty($_GET["le"]))
    $EL = $_GET["le"];
$_GET["le"] = $EL;

$ER = 'avg';
if (!empty($_GET["re"]))
    $ER = $_GET["re"];
$_GET["re"] = $ER;

$W = 1200;
if (!empty($_GET["w"]))
    $W = $_GET["w"];
$_GET["w"] = $W;

$H = 800;
if (!empty($_GET["h"]))
    $H = $_GET["h"];
$_GET["h"] = $H;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// КОНВЕРТАЦИЯ
$rrate = 1;
$lrate = 1;
$lforexdata = [];
$rforexdata = [];
$convr = false;
$convl = false;
//if (($EL == 'bitfinex') || ($EL == 'binance') )
//    $TOL = 'USD';

if (($TOL != 'RUB' && $TOL != 'RUR')) {
    $url = $forex . "?fsym=$TOL&tsym=RUB&limit=$LIMIT";
    $feed = file_get_contents(
        $url, false, stream_context_create($options));
    $fd = json_decode($feed, true);
    if ($fd['Response'] == 'Error') {
        ploterr("Error: get (left) FOREX data from Cryptocompare\n" . "url: " . $url . "\n" . "Message: " . $ldata['Message']);
        exit;
    }
    $fd = $fd['Data'];
    foreach ($fd as $v) {
        $fp = ($v['open'] + $v['low'] + $v['close']) / 3.0;
        $lforexdata[$v['time']] = $fp;
    }
    if ($TOL == 'USD' || $TOL == 'EUR') {
        //последнее значение берем от https://dealsbot.tk/manyapi/api/ratesForex
        $aHTTP = array(
            'http' => // Обертка, которая будет использоваться
                array(
                    'method' => 'GET', // Метод запроса
                    // Ниже задаются заголовки запроса
                    'header' => 'Authorization:' . $token->token,
                    'timeout' => 20
                )
        );
        $u0 = $dnforex;
        $f0 = file_get_contents(
            $u0, false, stream_context_create($aHTTP));
        $fd0 = json_decode($f0, true);
        $lforexdata[$v['time']] = ($TOL == 'USD') ? $fd0['usd_rub']['last'] : $fd0['eur_rub']['last'];
    }
    $convl = true;

}

if ($TOR != 'RUB' && $TOR != 'RUR') {
    $url = $forex . "?fsym=$TOR&tsym=RUB&limit=$LIMIT";
    $feed = file_get_contents(
        $url, false, stream_context_create($options));
    $fd = json_decode($feed, true);
    if ($fd['Response'] == 'Error') {
        ploterr("Error: get (right) FOREX data from Cryptocompare\n" . "url: " . $url . "\n" . "Message: " . $ldata['Message']);
        exit;
    }
    $fd = $fd['Data'];
    foreach ($fd as $v) {
        $fp = ($v['open'] + $v['low'] + $v['close']) / 3.0;
        $rforexdata[$v['time']] = $fp;
    }
    if ($TOR == 'USD' || $TOR == 'EUR') {
        //последнее значение берем от https://dealsbot.tk/manyapi/api/ratesForex
        $aHTTP = array(
            'http' => // Обертка, которая будет использоваться
                array(
                    'method' => 'GET', // Метод запроса
                    // Ниже задаются заголовки запроса
                    'header' => 'Authorization:' . $token->token,
                    'timeout' => 20
                )
        );
        $u0 = $dnforex;
        $f0 = file_get_contents(
            $u0, false, stream_context_create($aHTTP));
        $fd0 = json_decode($f0, true);
        $rforexdata[$v['time']] = ($TOR == 'USD') ? $fd0['usd_rub']['last'] : $fd0['eur_rub']['last'];
    }
    $convr = true;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//if ($EL == 'bitfinex') {
//    $TOL = 'USD';
//}
//
//if ($EL == 'binance') {
//    $TOL = 'USDT';
//}
//
//if ($EL == 'yobit' && $TOL == 'RUB')
//    $TOL = 'RUR';
//----------------------------------------------------------------------------------------------------------------------
//получаем данные с cryptocompare
$url = $cc . "?fsym=$FROML&tsym=$TOL&limit=$LIMIT&e=$EL";
$feed = file_get_contents(
    $url, false, stream_context_create($options)
);
$ldata = json_decode($feed, true);
if ($ldata['Response'] == 'Error') {
    ploterr("Error: get (left) data from Cryptocompare\n" . "url: " . $url . "\n" . "Message: " . $ldata['Message']);
    exit;
}


if (strtoupper($ER) == 'AVG') {
//получаем наши данные
    $startDate = $ldata['Data'][0]['time'];

    $url = $cl . "?startDate=$startDate";
    $aHTTP = array(
        'http' => // Обертка, которая будет использоваться
            array(
                'method' => 'GET', // Метод запроса
                // Ниже задаются заголовки запроса
                'header' => 'Authorization:' . $token->token,
                'timeout' => 20
            )
    );
    $feed = file_get_contents(
        $url, false, stream_context_create($aHTTP)
    );
    $rdata = json_decode($feed, true);
    if (count($rdata) == 0) {
        ploterr("Error: get AVG date\nurl: $url\n");
        exit;
    }
}

if (strtoupper($ER) == 'CKZ') {
    $startDate = $ldata['Data'][0]['time'];
    $url = $ckz . "?startDate=$startDate";
    $aHTTP = array(
        'http' => // Обертка, которая будет использоваться
            array(
                'method' => 'GET', // Метод запроса
                // Ниже задаются заголовки запроса
                'header' => 'Authorization:' . $token->token,
                'timeout' => 20
            )
    );
    $feed = file_get_contents(
        $url, false, stream_context_create($aHTTP)
    );
    $rdata = json_decode($feed, true);
    if (count($rdata) == 0) {
        ploterr("Error: get CKZ date\nurl: $url\n");
        exit;
    }
}

if (!(strtoupper($ER) == 'AVG' || strtoupper($ER) == 'CKZ')) {
    //получаем данные с cryptocompare
    $url = $cc . "?fsym=$FROMR&tsym=$TOR&e=$ER&limit=$LIMIT";
    $feed = file_get_contents(
        $url, false, stream_context_create($options)
    );
    $rdata = json_decode($feed, true);

    if ($rdata['Response'] == 'Error') {
        ploterr("Error: get (right) data from Cryptocompare\n" . "url: " . $url . "\n" . "Message: " . $rdata['Message']);
        exit;
    }
    $rdata = $rdata['Data'];
    foreach ($rdata as $k => $v) {
        if ($convr) {
            $rrate = $rforexdata[$v['time']];
            //если такого значения нет, то потом заполним, а пока пропускаем
            if (empty($rrate)) continue;
        }

        $p = ($v['open'] + $v['low'] + $v['close']) / 3.0;
        $rdata[$k]['price'] = $p * $rrate;
    }
} else {
    $ER = strtoupper($ER) == 'AVG' ? 'СК' : 'ЦКЗ';
}
//----------------------------------------------------------------------------------------------------------------------
//нормализация данных
$m1 = [];

foreach ($ldata['Data'] as $v) {
    if ($convl) {
        $lrate = $lforexdata[$v['time']];
        //если такого значения нет, то потом заполним, а пока пропускаем
        if (empty($lrate)) continue;
    }

    $p = ($v['open'] + $v['low'] + $v['close']) / 3.0;
    $m1[$v['time']]['cc'] = $p * $lrate;
}
//запомним первый ключ массива чтобы выровнять массивы слева
$first = $ldata['Data'][0]['time'];

foreach ($rdata as $i) {
    //if (is_null($m1[$i['time']]['cc'])) continue;
    //здесь магия - отсекаем инициализирующие данные
    if ($i['price'] < 10) continue;
    //если наши значения ушли дальше в прошлое - равняем по первому значению CC
    if ($i['time'] < $first) continue;
    $m1[$i['time']]['dv'] = $i['price'];
}


//отсортируем по ключам
ksort($m1);

//запомним первые значения массива, для заполнения пустых мест
$d1 = 0;
$d2 = 0;
foreach ($m1 as &$i) {
    if ($d1 == 0) {
        if (!empty($i['dv']))
            $d1 = $i['dv'];
    }
    if ($d2 == 0) {
        if (!empty($i['cc']))
            $d2 = $i['cc'];
    }
    if ($d1 != 0 && $d2 != 0) break;
}
//заполним пустые места массива
foreach ($m1 as &$i) {
    if (empty($i['dv']))
        $i['dv'] = $d2;
    else
        $d2 = $i['dv'];

    if (empty($i['cc']))
        $i['cc'] = $d1;
    else
        $d1 = $i['cc'];
}

//вычислим отклонение
$div = [];
foreach ($m1 as $k => $v) {
    $div[$k] = ($v['dv'] - $v['cc']) / $v['dv'] * 100.0;
    $m1[$k]['div'] = $div[$k];
}

//вычислим MA
$dv = $div;//array_reverse($div, TRUE);
$sm = trader_sma($dv, 24 * $MA);
$sma = [];
$dv = array_reverse($dv, TRUE);
$sm = array_reverse($sm, TRUE);
foreach ($dv as $k => $v) {
    $zv = array_shift($sm);
    if (is_null($zv)) break;
    $sma[$k] = $zv;
    $m1[$k]['ma'] = $zv;
}
//отсортируем по ключам
ksort($sma);

#строим коридор
$kk = [];
$dvt = $div;
$smt = $sma;//array_slice($sma,0,24*$MA, true);
foreach ($smt as $kl => $vl) {
    $kk[$kl] = avg(array($kl => $vl), $dvt, 24 * $MA);
    $m1[$kl]['kor1'] = $kk[$kl]['sum1'];
    $m1[$kl]['kor2'] = $kk[$kl]['sum2'];
    array_shift($dvt);
}
//$kk=array_reverse($kk, TRUE);
//$sma=array_reverse($sma, TRUE);

//отсортируем по ключам
ksort($kk);

//получим координаты последних значений
$lastX = array_key_last($m1);
$lastCC = $m1[$lastX]['cc'];
$lastDV = $m1[$lastX]['dv'];
$lastX = $lastX + 60 * 60;
$lastRData = $rdata[array_key_last($rdata)]['price'];
//----------------------------------------------------------------------------------------------------------------------
//если нужны данные - выводим
if ($MODE == 't') {
    header('Content-Type: application/json;charset=utf-8');
    $tm = array_key_last($m1);
    $mm = end($m1);
    $mm['time'] = $tm;
    $mm['lprice'] = $lastCC;
    $mm['rprice'] = $rdata[array_key_last($rdata)]['price'];
    $mm['le'] = $EL;
    $mm['re'] = $ER;
    $mm['lfrom'] = $FROML;
    $mm['rfrom'] = $FROMR;
    $mm['lc'] = $TOL;
    $mm['rc'] = $TOR;
    $data = json_encode($mm, JSON_UNESCAPED_UNICODE);
    echo($data);
    exit;
}

if ($MODE == 'd') {
    $data = json_encode($m1, JSON_UNESCAPED_UNICODE);
    echo ($data);
exit;
}
//сохраняем данные на диск
$f1fn = tempnam("./tmp", "f1_");
chmod($f1fn, 0666);
$f1 = fopen($f1fn, 'w+');
fwrite($f1, "#дата сс dv %отклонения ma\n");
foreach ($m1 as $k => $v) {
    $s = $k . ' ' . $v['cc'] . ' ' . $v['dv'] . ' ' . $v['div'] . ' ' . $v['ma'] . ' ' . $v['kor1'] . ' ' . $v['kor2'] . "\n";
    fwrite($f1, $s);
}
fwrite($f1, "\n\n");
fwrite($f1, "#секция последние значения\n");
$s = $k . ' ' . $v['cc'] . ' ' . $v['dv'] . ' ' . $v['div'] . ' ' . $v['ma'] . ' ' . $v['kor1'] . ' ' . $v['kor2'] . "\n";
fwrite($f1, $s);

//fwrite($f1, "\n\n");
//fwrite($f1, "#секция RData\n");
//foreach ($rdata as $v) {
//$s = $v['time'].' '.$v['price']."\n";
//    fwrite($f1, $s);
//}
//fwrite($f1, "\n\n");
//fwrite($f1, "#секция последние значения RData\n");
//fwrite($f1, $s);

fclose($f1);

$f2fn = tempnam("./tmp", "f2_");
chmod($f2fn, 0666);
$f2 = fopen($f2fn, 'w+');
fwrite($f2, "#секция с отклонениями\n");
foreach ($div as $k => $v) {
    $s = $k . ' ' . $v . "\n";
    fwrite($f2, $s);
}
$lastK = $k;
$lastDiv = $v;

//секция с скользящая
fwrite($f2, "\n\n");
fwrite($f2, "#секция скользящая\n");
foreach ($sma as $k => $v) {
    $s = $k . ' ' . $v . "\n";
    fwrite($f2, $s);
}
$lastSma = $v;

//секция с коридором
fwrite($f2, "\n\n");
fwrite($f2, "#секция с коридором\n");
foreach ($kk as $kl => $vl) {
    $s = $kl . ' ' . $vl['sum1'] . ' ' . $vl['sum2'] . "\n";
    fwrite($f2, $s);
}
$lastKor1 = $vl['sum1'];
$lastKor2 = $vl['sum2'];

//последние значения
fwrite($f2, "\n\n");
fwrite($f2, $lastK . ' ' . $lastDiv . ' ' . $lastSma . ' ' . $lastKor1 . ' ' . $lastKor2 . "\n");
fclose($f2);

//----------------------------------------------------------------------------------------------------------------------

//--------------------------------------------
$image_file = tempnam("./tmp", "gpout_");

$tz = 60 * 60 * 6;
$lt = array_key_last($m1);
$gnuplot_cmds = <<< GNUPLOTCMDS
set term png truecolor enhanced font arial 8 size $W, $H
#set term svg size $W, $H font "arial" dynamic mouse
set output "$image_file"
set title "Котировки BTC/RUB. Диапазон: $DAY д. Время: ".strftime("%H:%M", $lt+$tz)

#легенда
set key box
set key spacing 1.2
#set key width -14

#включаем правую ось
set y2tics 

#отключаем левую ось
#unset ytics

set style textbox opaque
set key box opaque left top

#set style fill transparent solid 0.5

set multiplot layout 2,1 scale 1,1

set lmargin 12
set rmargin 12
set ylabel "Цена, р."
set y2label "Цена, р."
set grid x y
set timefmt "%s"
set format x "%d/%m"
set xdata time

#тики каждый день
#set xtics 86400

unset ylabel
set format y ""

set xrange [time(0)-60*60*24*$DAY:time(0)+60*60*24]
#set label sprintf("%0.0f", $lastCC) at $lastX, $lastCC
#set label sprintf("%0.0f", $lastDV) at $lastX, $lastDV 

set pointsize 0.9
plot \
"$f1fn" index 0 using ($1+$tz):2 with lines lw 2 title (sprintf("$EL:$TOL*%0.2f: %0.0f р.", $lrate, $lastCC)), \
"" index 0 using ($1+$tz):3 with lines lw 2 title (sprintf('$ER:$TOR*%0.2f: %0.0f р.', $rrate, $lastDV)), \
"" index 1 using ($1+$tz):2:(sprintf("%0.0f", $2)) with labels point pt 7 lc rgb "red" boxed left offset 0.4,0 notitle, \
"" index 1 using ($1+$tz):3:(sprintf("%0.0f", $3)) with labels point pt 7 lc rgb "red" boxed left offset 0.4,0 notitle
#"" index 2 using ($1+$tz):2 with lines lw 2 lc rgb "#0000FF" title (sprintf('ЦКЗ: %0.0f р.', $lastRData))

unset title
set xlabel "Дата"
set key width -9
#set ylabel ""
unset ylabel
set y2label "отклонение"
set format y ""
set format y2 "%.2f%%"
#set yrange [-10:10]
#title (sprintf("коридор-(3%%): %0.2f%%",$lastKor1-0.3))
#title (sprintf("коридор+ (3%%): %0.2f%%",$lastKor2+0.3))

plot "$f2fn" index 0 using ($1+$tz):2 w p ls 2 pt 7 ps 0.5 lc rgb "#0026b0" title (sprintf('отклонение: %0.2f%%', $lastDiv)) \
     , "$f2fn" index 1 using ($1+$tz):2 with lines lw 2 lt rgb "#b84900" title (sprintf('SMA($MA d): %0.2f%%', $lastSma)) \
     , "$f2fn" index 2 using ($1+$tz):2 with lines lw 2 title (sprintf("коридор- : %0.2f%%",$lastKor1)) \
     , "$f2fn" index 2 using ($1+$tz):3 with lines lw 2  lc 2 title (sprintf("коридор+ : %0.2f%%",$lastKor2)) \
     , "$f2fn" index 3 using ($1+$tz):2:(sprintf("%0.2f", $2)) with labels point pt 7 lc rgb "red" boxed left offset 0.4,0 notitle \
     , "$f2fn" index 3 using ($1+$tz):3:(sprintf("%0.2f", $3)) with labels point pt 7 lc rgb "red" boxed left offset 0.4,0 notitle \
     , "$f2fn" index 3 using ($1+$tz):4:(sprintf("%0.2f", $4)) with labels point pt 7 lc rgb "red" boxed left offset 0.4,0 notitle \
     , "$f2fn" index 3 using ($1+$tz):5:(sprintf("%0.2f", $5)) with labels point pt 7 lc rgb "red" boxed left offset 0.4,0 notitle \
     , 0 with lines notitle lt rgb "#000000"
GNUPLOTCMDS;

$gnuplot_cmds .= "\n";


$descriptorspec = array(
    0 => array('pipe', 'r'),
    1 => array('pipe', 'w'),
    2 => array('pipe', 'r')
);

$process = proc_open('gnuplot', $descriptorspec, $pipes);

if (!is_resource($process)) {
    throw new \Exception('Unable to run GnuPlot');
}

$stdin = $pipes[0];
$stdout = $pipes[1];

fwrite($stdin, $gnuplot_cmds);

$gnuplot_cmds = "quit\n";
fwrite($stdin, $gnuplot_cmds);
proc_close($process);

header("Content-Type: image/png");
//header("Content-Type: image/svg+xml");
readfile($image_file);
//
// Clean up and exit
unlink($image_file);
unlink($f1fn);
unlink($f2fn);
?>